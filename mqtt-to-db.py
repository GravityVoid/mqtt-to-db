import paho.mqtt.client as mqtt
import pymongo
from settings import *

db_client = pymongo.MongoClient(db_addr, db_port)
db = db_client.test
db_collection = db.sensor

def on_connect(client, userdata, flags, rc):
    print("Connected with:", rc)

def on_message(client, userdata, msg):
    print(msg.topic, msg.payload)
    return_id = db_collection.insert_one({msg.topic: float(msg.payload)}).inserted_id
    print("Return id:", return_id)

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect(mqtt_broker_addr, mqtt_broker_port)
client.subscribe("sometest/rand")

client.loop_start()

while True:
    pass